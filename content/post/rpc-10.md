+++
date = "2016-09-26T14:21:36-05:00"
title = "rpc 10"

+++

## Problem set

A pdf can be found [here](https://github.com/pin3da/Programming-contest/raw/master/solved/Others/RPC-10-2016/Problemset.pdf)

## Link to solutions

- [B - Subsets](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/B.cc)
- [C - Cacho](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/C.cc)
- [D - Divisors](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/D.cc)
- [E - Laser Mirrors](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/E.cc)
- [F - Concat Fibos](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/F.cc)
- [G - Generate Sort and Search](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/G.cc)
- [H - Homework](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/H.cc)
- [I - Recurrences](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/I.cc)
- [J - Progressions](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/J.cc)
- [K - Kites](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-10-2016/K_cin.cc)

## Discussion

(Work in progress)

### I - Recurrences

This is a calssic problem on matrix exponentiation, [this](http://zobayer.blogspot.com.co/2010/11/matrix-exponentiation.html)
is a very cool post about that topic.
