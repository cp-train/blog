+++
date = "2016-09-26T14:31:50-05:00"
title = "rpc 11"

+++

## Problem set


## Link to solutions

- [A - Time Travel](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-11-2016/A.cc)
- [C - Containers](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-11-2016/C.cc)
- [D - Divisors](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-11-2016/D.cc)
- [F - Fusing Trees](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-11-2016/F.cc)
- [G - Go](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-11-2016/G.cc)
- [H - Huaauhahhuahau](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-11-2016/H.cc)
- [I - Isoceles](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-11-2016/I.cc)
- [L - Tiles](//github.com/pin3da/Programming-contest/blob/master/solved/Others/RPC-11-2016/L.cc)

## Discussion

(Work in progress)
